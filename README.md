***
# **CLASHIT**
#*http://clashit.herokuapp.com* 

### *A revolutionary way of aggregating opinions* ###
![Clashit](https://i.imgur.com/ZUMf3jF.png)

## SuperUser : **Bruce_Almighty** **12345** 
## Mlab : *https://mlab.com/databases/clashitoff*  **clashit** **1234567890qwertyuio** 

***
## Current functionality
- Authorization(Admin , User, Guest).
- Route protection.
- Displaying dashboard with followed rooms, latest subscriptions and top rooms.
- Pagination of followed rooms.
- Getting news articles from [NewsApi](https://newsapi.org/).
- Filtering news articles by category and substring.
- Leaving posts through modal window.
- Editing your posts.
- Removing your posts.
- Ability for admins to remove any post.
- Attaching image to a post(removing image from post upon editing).
- Admin panel, through which adding of custom rooms is implemented.
- Displaying profile of user, her latest posts.
- Ability to change profile cover.
***
##References
* [TZ](https://docs.google.com/document/d/1fi7YAtBBfRNXtb_92oqBIuoFBomhv2_S9EFCu45YRZ8/edit?usp=sharing)
* [Developer] : *artieherasymov@gmail.com*
