const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
const Room = require('../models/room');
const Post = require('../models/post');
const multer = require('multer');
const uniqid = require('uniqid');
let fs = require('fs');
const s3fs = require('s3fs');
const multiparty = require('connect-multiparty');
const multipartyMiddleware = multiparty();
const converter = require('base64-arraybuffer');
const sentimentAnalysis = require('sentiment-analysis');
const classifier = require('../logics/classifier.js');
const decisiontree = require('../logics/decisiontree.js');


const s3fsImpl = new s3fs('clashit', {
  accessKeyId: 'AKIAICENDCG6OQCXSPRA',
  secretAccessKey: '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'
});

s3fsImpl.create();

const s3fsPostImpl = new s3fs('clashit_posts', {
  accessKeyId: 'AKIAICENDCG6OQCXSPRA',
  secretAccessKey: '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'
});

s3fsPostImpl.create();

let DIR = './uploads/users/pictures';

let upload = multer({ dest: DIR }).single('photo');

router.use(multipartyMiddleware);


router.post('/register', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    status: 'User',
    posts: [],
    subscriptions: [],
    subscribers: []
  });

  User.addUser(newUser, (err, user) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else {
      res.json({ success: true, msg: 'User registered' });
    }
  });
});

router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    if (!user) {
      return res.json({ success: false, msg: 'User not found' });
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {
        const token = jwt.sign({ data: user }, config.secret, {
          expiresIn: 604800
        })

        res.json({
          success: true,
          token: 'JWT ' + token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email,
            status: user.status
          }
        });
      } else {
        return res.json({ success: false, msg: 'Wrong password' });
      }
    });
  });
});

router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  res.json({ user: req.user });
});

router.post('/profile', (req, res, next) => {
  console.log(req.headers.authorization);
  upload(req, res, function (err) {
    if (err) return res.json({ err });
    let path = req.files.file.path;

    let filename = req.files.file.originalFilename.split('.');
    filename = req.headers.authorization + '.' + filename[1];

    let stream = fs.createReadStream(path);
    return s3fsImpl.writeFile(filename, stream)
      .then(() => {
        fs.unlink(path, (err) => {
          if (err) return res.json({ err });
          User.updateCover(req.headers.authorization, filename, (err) => {
            return res.json({ success: true });
          })
        });
      })
      .catch((err) => res.json(err));
  });
});

router.post('/uploadPostImage', (req, res, next) => {

  upload(req, res, function (err) {
    if (err) return res.json({ err });
    let path = req.files.file.path;

    let filename = req.files.file.originalFilename.split('.');
    filename = req.headers.authorization + '.' + filename[1];
    let stream = fs.createReadStream(path);
    return s3fsPostImpl.writeFile(filename, stream)
      .then(() => {
        fs.unlink(path, (err) => {
          if (err) return res.json({ err });
          Post.updatePostCover(req.headers.authorization, filename, (err) => {
            return res.json({ success: true });
          });
        });
      });
  });
});

router.post('/admin', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  let newRoom = new Room({
    title: req.body.title,
    body: req.body.description,
    author: req.body.author,
    resource: req.body.urlToImage,
    category: req.body.category,
    posts: [],
    url: req.body.url,
    created_at: new Date()
  });

  Room.isAlreadyThere(newRoom.title, (err, data) => {
    if (data !== null) return res.json({ success: false, msg: 'Room is already there', id: data._id });
    Room.addRoom(newRoom, (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else {
        res.json({ success: true, data });
      }
    });
  });
});

router.post('/removeRoom', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  Room.removeRoom(req.body.roomId, (err, data) => {
    if (err) return res.json({ success: false, msg: err });
    return res.json({ success: true, msg: 'The room was successfully removed' });
  });
});

router.post('/dashboard', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Room.getLatest(req.body.ids, (err, latestRooms) => {
    if (err) res.json({ success: false })
    else res.json({ success: true, latestRooms: latestRooms });
  });
});

router.get('/room/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Room.getById(req.params.id, (err, data) => {
    if (err) res.json({ success: false })
    else res.json({ success: true, roomData: data });
  });
});

router.post('/post', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  console.log('isTopUser : ' + req.body.author.isTopUser);
  let newPost = new Post({
    id: uniqid(),
    header: req.body.header,
    body: req.body.body,
    author: {
      username: req.body.author.username,
      _id: req.body.author._id,
      isTopUser : req.body.author.isTopUser
    },
    image: null,
    room: req.body.room,
    category: req.body.category,
    sentiment: sentimentAnalysis(req.body.body)
  });
  if (newPost.sentiment < 0) newPost.sentiment *= -1;
  else if (newPost.sentiment > 0) newPost.sentiment += 1;

  let roomId;
  Room.addPost(newPost, (err, data) => {
    if (err) res.json({ success: false, msg: "Failed to add new post" });
    else {
      roomId = data._id;
      Post.addPost(newPost, (err) => {
        if (err) throw err;
        User.addPost(newPost, roomId, (err, user) => {
          User.getFootprint(user._id, (err, footprint) => {
            User.updateCircle(user._id, classifier.determineCircle(footprint), (err) => {
              if (err) return res.json({ err });
              res.json({ success: true, msg: 'Post has been added', id: newPost.id });
            })
          })

        });
      });
    }
  });
});

router.post('/removePost', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Room.removePostById(req.body.room, req.body.post, (err) => {
    if (err) res.json({ success: false, msg: err });
    else {
      Post.removeById(req.body.post, (err) => {
        if (err) res.json({ success: false, msg: err })
        res.json({ success: true, msg: 'Post has been deleted' });
      });
    }

  });
});

router.post('/updatePost', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Post.updateById(req.body.post, req.body.newHeader, req.body.image, req.body.newBody, (err) => {
    if (err) res.json({ success: false, msg: err });
    return res.json({ success: true, msg: "Object was updated" });
  });

});

router.get('/getUser/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.getUserById(req.params.id, (err, data) => {
    console.log(data);
    if (err) return res.json({ success: false, msg: err });
    s3fsImpl.readFile(data.cover)
      .then(file => {
        return res.json({ success: true, data: data, cover: converter.encode(file.Body) });
      })

  })
});

router.post('/subscribe', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.createSubscription(req.body.subscriber,
    req.body.subscription,
    (err) => {
      if (err) return res.json({ success: false, msg: err });
      User.createSubscriber(req.body.subscriber, req.body.subscription, (err) => {
        if (err) return res.json({ success: false, msg: err });
        return res.json({ success: true, msg: 'Subscription created' });
      });

    });
});

router.get('/getSubs/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.getLatestSubs(req.params.id, (err, data) => {
    if (err) return res.json({ success: false, msg: err });
    return res.json({ success: true, subs: data });
  });
});

router.post('/getSubPosts', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  Post.getLatestSubs(req.body.idArray, (err, data) => {
    if (err) return res.json({ success: false, msg: err });
    return res.json({ success: true, data });
  });
});

router.post('/getPosts', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Post.getPostsByIds(req.body.postIds, (err, data) => {
    let promises = [];
    let indexes = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].image != null) {
        promises.push(s3fsPostImpl.readFile(data[i].image));
        indexes.push(i);
      };
    }


    Promise.all(promises)
      .then(file => {
        let counter = 0;
        for (let i = 0; i < file.length; i++) {
          data[indexes[counter]].image = converter.encode(file[i].Body);
          counter++;
        }
        res.json({ success: true, data });
      })
      .catch(err => {
        res.json({ err });
      })
  })
}
);

router.post('/unsubscribe', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.removeSubscriptions(req.body.subscriber, req.body.subscription, (err) => {
    if (err) return res.json({ success: false, msg: err });
    User.removeSubscriber(req.body.subscription, req.body.subscriber, (err) => {
      if (err) return res.json({ success: false, msg: err });
      return res.json({ success: true, msg: 'Subscription has been successfully removed' });
    });
  });
});

router.get('/getTopRooms', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Room.getTopRooms((err, data) => {
    if (err) return res.json({ success: false, msg: err });
    data = data.slice(0, 10);
    return res.json({ success: true, data: data });
  });
});

router.get('/getFollowing/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.getFollowing(req.params.id, (err, data) => {
    if (err) return res.json({ success: false, msg: err });
    Room.getFollowed(data, (err, data) => {
      if (err) return res.json({ success: false, msg: err });
      return res.json({ success: true, data: data });
    });
  });
});

router.post('/like', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Post.addLike(req.body.postId, (err, data) => {
    User.addLiked(data.author._id, (err, data) => {
      if (err) return res.json({ success: false, msg: err });
      if (data.liked > 100 && data.isTopUser == false) {
        User.changeStatus(data._id, (err, data) => {
          if (err) return res.json({ err });
          User.addLike(req.body.userId, req.body.postId, (err, data) => {
            if (err) return res.json({ success: false, msg: err });
          });
          return res.json({ success: true, msg: "Like added" });
        })
      }
      User.addLike(req.body.userId, req.body.postId, (err, data) => {
        if (err) return res.json({ success: false, msg: err });
      });
      return res.json({ success: true, msg: "Like added" });
    });
  });
});

router.post('/report', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  Post.reportPost(req.body.id, (err, data) => {

    User.addReport(req.body.user, req.body.id, (err, data) => {
      if (err) res.json({ status: false, msg: err });
      if (data.reports + 1 >= 3) {
        Post.removeById(data._id, (err) => {
          if (err) return res.json({ status: false, msg: err });
          return res.json({ status: true });
        })
      } else {
        return res.json({ success: true, msg: 'Your report will be accounted for' });
      }
    });
  });
});


router.get('/getCoefficient/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  let footprint;

  User.getFootprint(req.params.id, (err, data) => {
    if (err) return res.json({ err });
    return res.json({ coefficient: classifier.determineCircle(data) });
  });
});

router.get('/getSortedSentiment/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.getFootprint(req.params.id, (err, data) => {
    if (err) return res.json({ err });
    return res.json({ sortedSentimentArray: classifier.getHighestCoefficients(data) });
  });
});

router.post('/updateProfile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.updateProfile(req.body.data, (err, data) => {
    if (err) res.json({ success: false, error: err });
    return res.json({ success: true, data: data })
  });
});

router.get('/getPosts/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  User.getPosts(req.params.id, (err, data) => {
    Post.getPostsByIds(data, (err, data) => {
      if (err) return res.json({ success: false, msg: err });
      return res.json({ success: true, data: data })
    });
  });
});

router.get('/getRecommendedUsers/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  let circle;
  let user;
  User.getUserById(req.params.id, (err, data) => {
    if (err) return res.json({ success: false, msg: err });
    user = data;
    circle = data.circle;
    User.findByCircle(data.circle, (err, neighbors) => {
      if (err) return res.json({ success: false, msg: err });
      return res.json({neighbors}):
    });
  })
});

module.exports = router;