// Returns a code that determines a circle to which user belongs
module.exports.determineCircle = function(footprint) {

    let technology = footprint[0].technology.sum / footprint[0].technology.quantity;
    let sport = footprint[0].sport.sum / footprint[0].sport.quantity;
    let politics = footprint[0].politics.sum / footprint[0].politics.quantity;
    let entertainment = footprint[0].entertainment.sum / footprint[0].entertainment.quantity;
    let science = footprint[0].science.sum / footprint[0].science.quantity;
    let music = footprint[0].music.sum / footprint[0].music.quantity;
    let health = footprint[0].health.sum / footprint[0].health.quantity;
    let gaming = footprint[0].gaming.sum / footprint[0].gaming.quantity;
    
    let footprintCode = '';

    if(technology > 1) footprintCode += '1';
    else footprintCode += '0';

    if(sport > 1) footprintCode += '1';
    else footprintCode += '0';

    if(politics > 1) footprintCode += '1';
    else footprintCode += '0';

    if(entertainment > 1) footprintCode += '1';
    else footprintCode += '0';

    if(science > 1) footprintCode += '1';
    else footprintCode += '0';

    if(music > 1) footprintCode += '1';
    else footprintCode += '0';

    if(health > 1) footprintCode += '1';
    else footprintCode += '0';

    if(gaming > 1) footprintCode += '1';
    else footprintCode += '0';
    console.log('footprint : ' + footprintCode);
    return parseInt(footprintCode , 2);
};

function compare(objA , objB) {
    if(objA.value < objB.value) return 1;
    else if(objA.value == objB.value) return 0;
    else return -1;
};

// Returns two categories with highest average sentiment score
module.exports.getHighestCoefficients = function(footprint) {
    let technology;
    let sport;
    let politics;
    let entertainment;
    let science;
    let music;
    let health;
    let gaming;
  
    if(footprint[0].technology.quantity == 0) technology = 0
    else technology = footprint[0].technology.sum / footprint[0].technology.quantity;


    if(footprint[0].sport.quantity == 0) sport = 0;
    else sport = footprint[0].sport.sum / footprint[0].sport.quantity;
    
    if(footprint[0].politics.quantity == 0) politics = 0;
    else politics = footprint[0].politics.sum / footprint[0].politics.quantity;

    if(footprint[0].entertainment.quantity == 0) entertainment = 0;
    else entertainment = footprint[0].entertainment.sum / footprint[0].entertainment.quantity;

    if(footprint[0].science.quantity == 0) science = 0;
    else science = footprint[0].science.sum / footprint[0].science.quantity;

    if(footprint[0].music.quantity == 0) music = 0;
    else music = footprint[0].music.sum / footprint[0].music.quantity;

    if(footprint[0].health.quantity == 0) health = 0;
    else health = footprint[0].health.sum / footprint[0].health.quantity;

    if(footprint[0].gaming.quantity == 0) gaming = 0; 
    else gaming = footprint[0].gaming.sum / footprint[0].gaming.quantity;
    

    let sentimentArray = [
        {category : 'technology' , value : technology}, 
        {category : 'sport',  value : sport} , 
        {category:  'politics', value : politics} , 
        {category :'entertainment', value : entertainment} ,
        {category : 'science', value : science},
        {category : 'music', value : music},
        {category: 'health' , value : health},
        {category : 'gaming' ,value : gaming}
    ];

    sentimentArray.sort(compare).slice(0,2);


    console.log(sentimentArray);
    return {primeCategory : sentimentArray[0] , secondaryCategory: sentimentArray[1]};
};