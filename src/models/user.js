const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const UserSchema = mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  cover: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: 'A stand up guy who is apparently too busy to fill his description section. Shame on him. Subscribe to him though, he rocks!'
  },
  status: {
    type: String,
    required: true
  },
  isTopUser:{
    type:Boolean,
    required: true,
    default:false
  },
  posts: {
    type: Array,
    default: []
  },
  subscribers: {
    type: Array,
    default: []
  },
  subscriptions: {
    type: Array,
    default: []
  },
  following: {
    type: Array,
    default: []
  },
  likes: {
    type: Array,
    default: []
  },
  liked : {
    type:Number,
    default:0,
    required:true
  },
  reported : {
    type:Array,
    default: []
  },
  circle : {
    type: Number,
    required: true,
    default:0
  },
  footprint: {
    technology: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      },
    },
    sport: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    politics: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    entertainment: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    science: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    music: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    health: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    },
    gaming: {
      quantity: {
        type: Number,
        default: 0
      },
      sum: {
        type: Number,
        default: 0
      }
    }
  }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function (id, callback) {
  User.findById(id, callback);
};

module.exports.getUserByUsername = function (username, callback) {
  const query = { username: username }
  User.findOne(query, callback);
};

module.exports.addUser = function (newUser, callback) {
  console.log(newUser);
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};

module.exports.comparePassword = function (candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if (err) throw err;
    callback(null, isMatch);
  });
};

module.exports.createSubscription = function (subscriber, subscription, callback) {
  User.update({ '_id': subscriber },
    {
      $push: {
        'subscriptions': subscription
      },
    }
    ,
    callback);
};

module.exports.createSubscriber = function (subscriber, subscription, callback) {
  User.update({ '_id': subscription },
    {
      $push: {
        'subscribers': subscriber
      }
    },
    callback)
};

module.exports.getLatestSubs = function (userId, callback) {
  User.distinct('subscriptions', { '_id': userId }, callback);
};

module.exports.addPost = function (post, roomId, callback) {

  switch (post.category) {
    case 'technology':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.technology.sum': post.sentiment, 'footprint.technology.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'sport':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.sport.sum': post.sentiment, 'footprint.sport.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'politics':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.politics.quantity': 1, 'footprint.politics.sum': post.sentiment }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'entertainment':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.entertainment.sum': post.sentiment, 'footprint.entertainment.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'science':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.science.sum': post.sentiment, 'footprint.science.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'music':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.music.sum': post.sentiment, 'footprint.music.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'health':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.health.sum': post.sentiment, 'footprint.health.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
    case 'gaming':
      User.findOneAndUpdate(post.author, {
        $push: { 'posts': post._id, 'following': roomId },
        $inc: { 'footprint.gaming.sum': post.sentiment, 'footprint.gaming.quantity': 1 }
      },
        { safe: true, upsert: true }, callback);
      break;
  }
};

module.exports.removeSubscriptions = function (subscriberId, subscriptionId, callback) {
  User.update({ '_id': subscriberId }, { $pull: { 'subscriptions': subscriptionId } }, callback);
};

module.exports.removeSubscriber = function (subscriberId, subscriptionId, callback) {
  console.log('Subscription to remove : ' + subscriptionId);
  console.log('Subscriber : ' + subscriberId);
  User.update({ '_id': subscriptionId }, { $pull: { 'subscribers': subscriberId } }, callback);
};

module.exports.getFollowing = function (userId, callback) {
  User.distinct('following', { '_id': userId }, callback);
};

module.exports.addLike = function (userId, postId, callback) {
  User.findByIdAndUpdate(userId, { $push: { 'likes': postId } }, callback);
};

module.exports.updateCover = function (userId, covername, callback) {
  User.findByIdAndUpdate(userId, { $set: { 'cover': covername } }, callback);
};

module.exports.getFootprint = function (id, callback) {
  User.distinct('footprint', { '_id': id }, callback);
};

module.exports.updateProfile = function (data, callback) {
  User.findByIdAndUpdate(data.id,
    {
      $set: {
        'name': data.name,
        'email': data.email
      }
    }
    , callback)
};

module.exports.getPosts = function(id , callback) {
  User.distinct('posts' , {'_id':id}, callback);
};

module.exports.findByCircle = function(circle , callback) {
  User.distinct('_id' , {'circle' : circle} , callback);
};

module.exports.updateCircle = function(id , newCircle , callback) {
  console.log('Updating user : ' + id);
  console.log('Setting circle : ' + newCircle);
  User.findByIdAndUpdate(id , {'circle' : newCircle} , callback);
};

module.exports.addLiked = function(id , callback) {
  User.findByIdAndUpdate(id , {$inc:{'liked' : 1}} , callback);
};

module.exports.changeStatus = function(id , callback) {
  User.findByIdAndUpdate(id, {$set : {'isTopUser': true}});
};

module.exports.addReport = function(id , postId ,callback) {
  console.log('Adding report to user : ' + id);
  console.log('Reported post : ' + postId);
  User.findByIdAndUpdate(id , {$push : {'reported' : postId}} , callback);
};


module.exports.findTopUsers = function(callback) {
  User.distinct('_id' , {'isTopUser' : true} , callback);
};