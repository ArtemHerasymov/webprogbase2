const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');


const PostSchema = mongoose.Schema({
    id: {
        type:String,
        required:true
    },
    header: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    author: {
       username: {type:String, required: true},
       _id: {type:String, required:true},
       isTopUser:{type:Boolean , required:true}
    },
    image: {
        type:String,
        default:null
    },
    room: {
        type: String,
        required: true
    },
    likes : {
        type: Number,
        required : true,
        default: 0
    }, 
    references: {
        type:Array,
        default:[]
    },
    reports:{
        type: Number,
        required: true,
        default : 0
    },
    sentiment: {
        type: Number,
        required: true,
        default:0
    },
    category: {
        type: String,
        required: true,
        default: 'general'
    },
    created_at:{
        type: Date,
        required:true
    }
});

const Post = module.exports = mongoose.model('Post', PostSchema);

module.exports.getPostById = function (id, callback) {
    Post.findById(id, callback);
};

module.exports.getPostByTitle = function (title, callback) {
    const query = { title: title };
    Post.findOne(query, callback);
};

module.exports.addPost = function (newPost, callback) {
    newPost.created_at = new Date();
    newPost.save(callback);
};

module.exports.removeById = function (id, callback) {
    Post.remove({ _id: id }, callback);
};

module.exports.updateById = function (id, newHeader, newImage, newBody, callback) {
    Post.findOneAndUpdate({id:id},
        { $set: { 'header': newHeader, 'body': newBody, 'image':newImage } },
        { safe: true, upsert: true },
        callback);
};

module.exports.getLatestSubs = function(idArray, callback) {
    Post.find({'author._id': {$in: idArray}}, callback);
};

module.exports.getPostsByIds = function(ids, callback) {
    Post.find({_id:{$in:ids}}, callback);
};

module.exports.addLike = function(id , callback) {
    Post.findByIdAndUpdate(id , {$inc:{'likes' : 1}} , callback);
};

module.exports.reportPost = function(id, callback) {
    Post.findByIdAndUpdate(id , {$inc:{'reports' : 1}} , callback);
};

module.exports.updatePostCover = function(id, picture , callback) {
    console.log('updating : ' + id + " with : " + picture);
    Post.findOneAndUpdate({'id' : id} , {$set:{'image' : picture}} , callback);
}; 
