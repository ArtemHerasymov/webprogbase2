const mongoose = require('mongoose');
const config = require('../config/database');

const RoomSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    resource: {
        default: 'https://www.hopkinsmedicine.org/sebin/h/u/news.jpg',
        type: String
    },
    category: {
        type: String,
        required: true
    },
    posts: {
        type: Array
    },
    url: {
        type: String
    },
    numberOfPosts: {
        type: Number
    },
    created_at: {
        type: String,
        required: true
    }
});

const Room = module.exports = mongoose.model('Room', RoomSchema);

module.exports.addRoom = function (newRoom, callback) {
    newRoom.numberOfPosts = 0;
    newRoom.save(callback);
};

module.exports.getLatest = function (callback) {
    Room.find({}, callback);
};

module.exports.getById = function (id, callback) {
    Room.findById(id, callback);
};

module.exports.addPost = function (post, callback) {
    Room.findByIdAndUpdate(post.room,
        { $push: { "posts": post._id }, $inc: { "numberOfPosts": 1 } },
        callback);
};

module.exports.removePostById = function (roomId, postId, callback) {
    Room.findByIdAndUpdate({ '_id': roomId },
        { $pull: { 'posts': { '_id': postId } } },
        callback);
};

module.exports.isAlreadyThere = function (title, callback) {
    Room.findOne({ 'title': title }, callback);
};

module.exports.removeRoom = function (roomId, callback) {
    Room.findOneAndRemove(roomId, callback);
};

module.exports.getTopRooms = function(callback) {
    Room.find({},null, {sort:'-numberOfPosts'} , callback);
};

module.exports.getFollowed = function(ids , callback) {
    Room.find({_id:{$in:ids}} , callback);
};
