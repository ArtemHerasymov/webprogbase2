import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../services/loader.service';
import { AuthenticationService } from '../../services/authentication.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FileUploader, FileItem } from 'ng2-file-upload/ng2-file-upload';

const URL = 'http://localhost:3000/users/uploadPostImage';


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})


export class RoomComponent implements OnInit {

  id: String;
  private sub: any;
  roomData;
  dataArrivalFlag: Boolean;
  regularPosts;
  image;
  topPosts;

  header: String;
  body: String;
  editedPost;

  p: number = 1;
  path;
  isRemoved: Boolean;
  isSelected: Boolean;

  user;
  currectLikedPost;

  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'file' });


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loaderService: LoaderService,
    private authenticationService: AuthenticationService,
    private flashMessage: FlashMessagesService,
    private zone: NgZone
  ) {
    this.dataArrivalFlag = false;

  }

  ngOnInit() {
    this.topPosts = [];
    this.isRemoved = false;
    this.isSelected = false;
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false };

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'].toString();
    });

    this.loaderService.getRoomById(this.id).subscribe(data => {
      console.log(data);
      this.roomData = data.roomData;
      this.loaderService.getPostsById(this.roomData.posts).subscribe(data => {
        console.log(data);
        this.regularPosts = data.data.reverse();
        for(let i = 0, j=0 ; i < this.regularPosts.length; i++) {
          if(this.regularPosts[i].author.isTopUser){
            this.topPosts[j] = this.regularPosts[i];
            j++;
          } 
        }
        
        this.dataArrivalFlag = true;
      });
    });

    // Getting user profile to check out his likes
    this.loaderService.getUserById(this.authenticationService.getCurrentUser()).subscribe(data => {
      this.user = data.data;
    });
  }

  onPostSubmit() {

    if (!this.header || !this.body) {
      this.flashMessage.show('Please, fill all the fields', { cssClass: 'alert-danger', timeout: 3000 });
    } else {

      if (this.isSelected) {
        const post = {
          header: this.header,
          body: this.body,
          author: { username: JSON.parse(localStorage.getItem('user')).username, _id: JSON.parse(localStorage.getItem('user')).id, isTopUser: this.user.isTopUser },
          room: this.id,
          category: this.roomData.category
        };
        this.loaderService.addPost(post).subscribe(res => {
          this.uploader.authToken = res.id;
          console.log(res.id);
          this.uploader.uploadAll();
        });
      } else {
        const post = {
          header: this.header,
          body: this.body,
          author: { username: JSON.parse(localStorage.getItem('user')).username, _id: JSON.parse(localStorage.getItem('user')).id, isTopUser: this.user.isTopUser },
          room: this.id,
          category: this.roomData.category
        };
        this.loaderService.addPost(post).subscribe(res => {
          this.zone.runOutsideAngular(() => {
            location.reload();
          });
        });
      }
    }
  };

  onRemoveSubmit(postId) {
    this.loaderService.removePost(this.roomData._id, postId).subscribe(res => {
      this.zone.runOutsideAngular(() => {
        location.reload();
      });
    });
  };

  setValue(index) {
    this.header = this.regularPosts[index].header;
    this.body = this.regularPosts[index].body;
    this.image = this.regularPosts[index].image;
    this.editedPost = this.regularPosts[index].id;
  };

  onEditSubmit() {

    if (!this.header || !this.body) {
      this.flashMessage.show('Please,fill all the fields', { cssClass: 'alert-danger', timeout: 3000 });
    } else {
      if (this.isRemoved) this.image = null;
      this.loaderService.updatePost(this.id, this.editedPost, this.image, this.body, this.header).subscribe(res => {
        this.uploader.authToken = this.editedPost;

        this.uploader.uploadAll();
        this.loaderService.getPostsById(this.roomData.posts).subscribe(data => {
          this.regularPosts = data.data.reverse();
          this.dataArrivalFlag = true;
          this.zone.runOutsideAngular(() => {
            location.reload();
          });
        });

      });
    }
  };

  onLikeClicked(postId) {
    this.dataArrivalFlag = false;
    this.loaderService.addLike(postId).subscribe(res => {
      this.loaderService.getPostsById(this.roomData.posts).subscribe(data => {
        this.regularPosts = data.data.reverse();
        this.loaderService.getUserById(this.authenticationService.getCurrentUser()).subscribe(data => {
          console.log(data.data);

          this.user = data.data;
        });
        this.dataArrivalFlag = true;
      });
    });
  };

  onReportClicked(postId) {
    this.dataArrivalFlag = false;
    this.loaderService.reportPost(postId).subscribe(res => {
      this.loaderService.getPostsById(this.roomData.posts).subscribe(data => {
        this.regularPosts = data.data.reverse();

        this.loaderService.getUserById(this.authenticationService.getCurrentUser()).subscribe(data => {
          console.log(data.data);
          this.user = data.data;
          this.dataArrivalFlag = true;
        });
        this.dataArrivalFlag = true;
      });
    });
  };

  ngOnDestroy() {
    this.sub.unsubscribe();
  };

  terminateRoom() {
    this.loaderService.removeRoom(this.id).subscribe(res => {
      console.log(res);
      if (res.success) {
        this.flashMessage.show('Room was successfully removed', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['dashboard']);
      } else {
        this.flashMessage.show('Error occured', { cssClass: 'alert-danger', timeout: 3000 });
      }
    })
  };

  imageRemoved() {
    this.image = null;
    this.isRemoved = true;
  };

  isLiked(id) {
    this.currectLikedPost = id;
    if (this.user.likes.indexOf(id) == -1) return false;
    return true;
  };

  isReported(id) {
    if (this.user.reported.indexOf(id) == -1) return false;
    return true;
  }

}
