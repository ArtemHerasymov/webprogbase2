import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;


  constructor(
    private validateService: ValidateService, 
    private FlashMessage: FlashMessagesService,
    private authenticationService: AuthenticationService,
    private router : Router
  ) { 

  }

  ngOnInit() {
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password
    };

    if(!this.validateService.validateRegister(user)) {
      this.FlashMessage.show('Please fill all the fields', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    if(!this.validateService.validateEmail(user.email)) {
      this.FlashMessage.show('Enter valid email', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    };

    if(!this.validateService.validatePassword(user.password)) {
      this.FlashMessage.show('Password should be at least 8 characters long', {cssClass:"alert-danger" , timeout:3000});
      return false;
    }

    //Register user
    this.authenticationService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.FlashMessage.show('Registration successful', {cssClass: 'alert-success', timeout: 3000});        
        this.router.navigate(['/profile']);
      } else {
        console.log(data.msg);
        this.FlashMessage.show('Error occurred', {cssClass: 'alert-success', timeout: 3000});        
        this.router.navigate(['/register']);
      }
    });
  }
}
