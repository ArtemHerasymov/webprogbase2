import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LoaderService } from '../../services/loader.service';
import { ApiHandlerService } from '../../services/apihandler.service';;


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  title: String;
  body: String;
  author: String;
  url: String;
  suggestionArray;
  dataArrivalFlag;
  currentCategory: String;

  constructor(
    private validateService: ValidateService,
    private FlashMessage: FlashMessagesService,
    private LoaderService: LoaderService,
    private router: Router,
    private apiHandlerService: ApiHandlerService
  ) { }

  ngOnInit() {
    this.dataArrivalFlag = false;
  }

  onCreateSubmit() {
    const room = {
      title: this.title,
      body: this.body,
      author: this.author,
      url: this.url
    };
    console.log(room);

    if (!this.validateService.validateRoom(room)) {
      this.FlashMessage.show('Please fill all the fields', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    if (!this.validateService.validateHttp(room)) {
      room.url = 'http://' + room.url;
    };

    //Add new room
    this.LoaderService.addRoom(room).subscribe(data => {
      if (data.success) {
        this.FlashMessage.show('Room was successfully created', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/admin']);
      } else {
        this.FlashMessage.show("Error occurred", { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/admin']);
      }
    });
  }

  onApiDataRequested(category) {
    let articles = {};
    this.currentCategory = category;
    if (category) {
      this.apiHandlerService.getArticlesByCategory(category).subscribe((res) => {
        articles = res;
        console.log(res);
        this.suggestionArray = articles;
        this.dataArrivalFlag = true;
      });
    } else {
      this.apiHandlerService.makeHttpRequest().subscribe((res) => {
        articles = res;
        console.log(res);
        this.suggestionArray = articles;
        this.dataArrivalFlag = true;
      }
      );
    }

  }

  postSelected(postId) {
    const room = {
      title: this.suggestionArray.article[postId].title,
      description: this.suggestionArray.article[postId].description,
      author: "admin",
      urlToImage: this.suggestionArray.article[postId].urlToImage,
      category: this.currentCategory,
      url: this.suggestionArray.article[postId].url
    };


    //Add new room
    this.LoaderService.addRoom(room).subscribe(data => {
      if (data.success) {
        this.FlashMessage.show('Room was successfully created', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/admin']);
      } else {
        this.FlashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/admin']);
      }
    });

  }

}
