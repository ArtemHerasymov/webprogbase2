import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;
  status: String;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }


  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password,
      status: this.status
    };

    this.authenticationService.authenticateUser(user).subscribe(data => {
      if (data.success) {
        this.authenticationService.storeUserData(data.token , data.user);
        this.flashMessage.show('You are logged in', {
          cssClass: 'alert-success',
          timeout: 3000
        });

        if(data.user.status === 'User'){
          this.router.navigate(['dashboard']);          
        } else if(data.user.status === 'Admin') {
          this.router.navigate(['admin']);
        }
        

      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 3000
        });
        this.router.navigate(['login']);

      }
    });
  }

}
