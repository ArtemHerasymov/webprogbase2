import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionManagerService } from '../../services/subscription-manager.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  profileOwner;
  profileOwnerId: String;
  user;
  id: String;
  private sub: any;
  cover;
  isAlreadySubscription: Boolean;
  latestPosts;
  dataArrivalFlag: Boolean;

  constructor(private loaderService: LoaderService,
    private subscriptionManagerService: SubscriptionManagerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.profileOwnerId = params['id'].toString();
      this.dataArrivalFlag = false;
    });

    //Getting the data about profile owner to form his profile html page
    this.loaderService.getUserById(this.profileOwnerId).subscribe(data => {
      this.profileOwner = data.data;
      this.cover = data.cover;
      this.loaderService.getPostsById(this.profileOwner.posts).subscribe(data => {
        this.latestPosts = data.data;
        this.dataArrivalFlag = true;

        // Getting subscribers of the user to determine if profile owner is already a subscription
        this.loaderService.getUserById(this.user.id).subscribe(data => {
          this.user = data.data;
          this.isAlreadySubscription = this.user.subscriptions.indexOf(this.profileOwnerId) == -1 ? false : true;
        });
      })
    });

    // Reading user token from localStorage
    this.user = JSON.parse(localStorage.getItem('user'));


  }

  onSubscribe() {
    console.log('on subscribe');
    this.subscriptionManagerService.createSubscription(this.authenticationService.getCurrentUser(), this.profileOwnerId).subscribe(res => {
      this.isAlreadySubscription = true;
      this.router.navigate(['']);
    });
  };


  onUnsubscribe() {
    this.subscriptionManagerService.removeSubscription(this.user._id, this.profileOwnerId).subscribe(res => {
      this.isAlreadySubscription = false;
      this.router.navigate(['']);
    });
  };

}