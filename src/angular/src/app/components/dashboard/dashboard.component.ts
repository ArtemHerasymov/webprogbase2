import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService } from '../../services/loader.service';
import { RecommendationService } from '../../services/recommendation.service'; 
import {ApiHandlerService} from '../../services/apihandler.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  latestRooms;
  dataArrivalFlag: Boolean;
  
  subscriptions;
  latestPosts;
  topRooms;
  recommendedRooms;

  userPosts;

  p: number = 1;


  constructor(
    private loaderService: LoaderService, 
    private recommendationService : RecommendationService,
    private apihandlerService: ApiHandlerService,
    private router : Router
  ) {
    this.dataArrivalFlag = false;
  }

  ngOnInit() {
    this.loaderService.getFollowing(JSON.parse(localStorage.getItem('user')).id).subscribe(data => {
      this.latestRooms = data.data.reverse();
    });

    // Getting ids of subscription-users
    this.loaderService.getLatestSubs(JSON.parse(localStorage.getItem('user')).id).subscribe(subs => {

      //Getting userdata based on ids
      this.loaderService.getLatestPosts(subs.subs).subscribe(data => {
        this.latestPosts = data.data.slice(0,5);
        this.dataArrivalFlag = true;
      });

      this.loaderService.getUserPosts(JSON.parse(localStorage.getItem('user')).id).subscribe(posts=>{
        this.userPosts = posts.data;
      });
      //Getting top rooms
      this.loaderService.getTopRooms().subscribe(data => {
        this.topRooms = data.data;
        let recs = [];
        this.recommendationService.getRecommendatedArticles().subscribe(data => {
         this.apihandlerService.getArticlesByCategory(data.sortedSentimentArray.primeCategory.category)
          .subscribe(res => {
            this.recommendedRooms = [];
            this.recommendedRooms[0] = res.article[0];
            this.recommendedRooms[1] = res.article[1];
            this.recommendedRooms[0].category = data.sortedSentimentArray.primeCategory.category;
            this.recommendedRooms[1].category = data.sortedSentimentArray.primeCategory.category;
            this.apihandlerService.getArticlesByCategory(data.sortedSentimentArray.secondaryCategory.category)
              .subscribe(res => {
                this.recommendedRooms[2] = res.article[0];
                this.recommendedRooms[3] = res.article[1];
                this.recommendedRooms[2].category = data.sortedSentimentArray.secondaryCategory.category;
                this.recommendedRooms[3].category = data.sortedSentimentArray.secondaryCategory.category;
                this.dataArrivalFlag = true;
                
              });
              this.recommendationService.getRecommendedUsers()
                .subscribe(res => console.log(res));
          })
        })
      })
    });
  };


  onRoomSelected(room) {
    room.author = 'Admin';
    this.loaderService.addRoom(room).subscribe(data => {
      if (!data.success) {
        console.log(data);
        this.router.navigate(['/room' , data.id]);
      } else {
        console.log(data);
        this.router.navigate(['/room', data.data._id]);
      }
    });
  };


}