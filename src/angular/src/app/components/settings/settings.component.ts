import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { LoaderService } from '../../services/loader.service';

const URL = 'http://localhost:3000/users/profile';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  user;
  dataArrivalFlag: Boolean;
  cover;

  newName;
  newEmail;
  newDescription;
  newTwitter;
  newGoogle;
  newLinkedin;
  newFacebook;

  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private loaderService: LoaderService
  ) { }

  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'file', authToken: this.authenticationService.getCurrentUser()});


  ngOnInit() {
    this.dataArrivalFlag = false;
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false };

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.loaderService.getUserById(this.authenticationService.getCurrentUser()).subscribe(profile => {
        this.user = profile.data;
        this.cover = profile.cover;
        this.dataArrivalFlag = true;
      })
    };

    this.loaderService.getUserById(this.authenticationService.getCurrentUser()).subscribe(data => {
      this.user = data.data;
      this.dataArrivalFlag = true;
      this.newName = data.data.name;    
      this.newEmail = data.data.email;
      this.newDescription = data.data.description;          
      this.cover = data.cover;
    },
      err => {
        console.log(err);
        return false;
      });


  }

  onUpdateClicked() {
    let newPost = {
      id: this.user._id,
      name:this.newName,
      email:this.newEmail,
      description:this.newDescription,
      twitter: this.newTwitter,
      facebook:this.newFacebook,
      linkedin:this.newLinkedin,
      google:this.newGoogle
    }

    this.loaderService.updateProfile(newPost).subscribe(res => {
      console.log(res);
    });


  }

}
