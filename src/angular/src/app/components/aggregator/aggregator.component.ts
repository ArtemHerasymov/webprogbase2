import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiHandlerService } from '../../services/apihandler.service';
import { LoaderService } from '../../services/loader.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-aggregator',
  templateUrl: './aggregator.component.html',
  styleUrls: ['./aggregator.component.css']
})
export class AggregatorComponent implements OnInit {

  tab = 1;
  latestRooms;
  dataArrivalFlag: Boolean;
  currentCategory;
  p: number = 1;
  title: String;
  body: String;
  url: String;
  currentUser;

  searchInput;

  constructor(
    private apiHandlerService: ApiHandlerService,
    private loaderService: LoaderService,
    private flashMessage: FlashMessagesService,
    private router: Router
  ) {
    this.dataArrivalFlag = false;
    this.currentCategory = 'general';
  }

  ngOnInit() {
    this.apiHandlerService.getArticlesByCategory(this.currentCategory).subscribe(res => {
      this.latestRooms = res;
      console.log(res);
      this.dataArrivalFlag = true;
    });
  }

  tabChanged(tab) {
    this.dataArrivalFlag = false;
    this.tab = tab;
    let articles = {};
    switch (this.tab) {
      case 1: this.currentCategory = 'general'; break;
      case 2: this.currentCategory = 'technology'; break;
      case 3: this.currentCategory = 'sport'; break;
      case 4: this.currentCategory = 'politics'; break;
      case 5: this.currentCategory = 'entertainment'; break;
      case 6: this.currentCategory = 'science-and-nature'; break;
      case 7: this.currentCategory = 'music'; break;
      case 8: this.currentCategory = 'health-and-medical'; break;
      case 9: this.currentCategory = 'gaming'; break;
    };

    this.apiHandlerService.getArticlesByCategory(this.currentCategory).subscribe(res => {
      articles = res;
      console.log(res);
      this.latestRooms = articles;
      this.dataArrivalFlag = true;
    });
  };

  onRoomSelected(room) {
    room.category = this.currentCategory;
    room.author = 'Admin';
    this.loaderService.addRoom(room).subscribe(data => {
      if (!data.success) {
        console.log(data);
        this.router.navigate(['/room' , data.id]);
      } else {
        console.log(data);
        this.router.navigate(['/room', data.data._id]);
      }
    })
  };

  renderSearchResult() {
    this.apiHandlerService.getArticlesBySubstring(this.searchInput).subscribe(res => {
      this.latestRooms = res;
      this.dataArrivalFlag = true;
    });
  }

}
