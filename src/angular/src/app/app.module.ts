import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';

import { RouterModule, Routes } from '@angular/router';

import { ValidateService } from './services/validate.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { LoaderService } from './services/loader.service';
import { ApiHandlerService } from './services/apihandler.service';
import {SubscriptionManagerService} from './services/subscription-manager.service';
import { RecommendationService } from './services/recommendation.service';

import { FlashMessagesModule } from 'angular2-flash-messages';
import { RoomComponent } from './components/room/room.component';

import { FileSelectDirective } from 'ng2-file-upload';
import { SettingsComponent } from './components/settings/settings.component';
import { AggregatorComponent } from './components/aggregator/aggregator.component';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'room/:id', component: RoomComponent, canActivate: [AuthGuard] },
  { path: 'profile/:id', component: ProfileComponent, canActivate: [AuthGuard] },
  { path:'settings' , component: SettingsComponent , canActivate: [AuthGuard]},
  { path:'aggregator' , component: AggregatorComponent , canActivate: [AuthGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    AdminComponent,
    RoomComponent,
    FileSelectDirective,
    SettingsComponent,
    AggregatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule,
    NgxPaginationModule,
    BrowserModule,
  ],
  providers: [ValidateService, AuthenticationService, AuthGuard, LoaderService, ApiHandlerService, AdminGuard, SubscriptionManagerService, RecommendationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
