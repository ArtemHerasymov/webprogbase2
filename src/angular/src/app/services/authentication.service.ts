import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthenticationService {

  authenticationToken: any;
  user: any;

  constructor(private http: Http) { }


  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
      .map(res => res.json());
  }


  authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
      .map(res => res.json());
  }

  getProfile() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authenticationToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/profile', { headers: headers })
      .map(res => res.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    user.cover = null;
    localStorage.setItem('user', JSON.stringify(user));

    this.authenticationToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authenticationToken = token;
    return token;
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  logout() {
    this.authenticationToken = null;
    this.user = null;
    localStorage.clear();
  }

  isAdmin() {
    if (JSON.parse(localStorage.getItem('user')).status == 'Admin') return true;
    else return false;
  }

  isAuthor(authorId) {
    if(JSON.parse(localStorage.getItem('user')).id == authorId) return true;
    else return false;
  };

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user')).id;
  };

}
