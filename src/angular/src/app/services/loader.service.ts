import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class LoaderService {

  room: any;
  authenticationToken;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService
  ) { }

  // Carries out POST-request to be caught on backend

  addRoom(room) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());
    return this.http.post('http://localhost:3000/users/admin', room, { headers: headers })
      .map(res => res.json());
  };

  getLatestRooms(ids) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/dashboard', { ids }, { headers: headers })
      .map(res => res.json());
  };

  getFollowing(userId) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/getFollowing/' + userId, { headers })
      .map(res => res.json());
  };

  getRoomById(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/room/' + id, {
      headers: headers,
    })
      .map(res => res.json());
  };

  addPost(post) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/post', post, { headers: headers })
      .map(res => res.json());
  };


  removePost(roomId, postId) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/removePost', { room: roomId, post: postId }, { headers: headers })
      .map(res => { res.json() });
  };

  updatePost(roomId, postId, newImage, newBody, newHeader) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/updatePost', { room: roomId, post: postId, image: newImage, newBody, newHeader }, { headers: headers })
      .map(res => { res.json() });
  };

  getUserById(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/getUser/' + id, { headers: headers })
      .map(res => res.json());
  };


  getLatestSubs(userId) {
    console.log(userId);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/getSubs/' + userId, { headers: headers })
      .map(res => res.json());
  };

  getLatestPosts(idArray) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/getSubPosts', { idArray }, { headers })
      .map(res => res.json());
  };

  getPostsById(ids) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/getPosts', { postIds: ids }, { headers: headers })
      .map(res => res.json());
  };

  sendImageToDb(img, user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/profile', { img: img, user: user }, { headers: headers })
      .map(res => res.json());
  };


  removeRoom(roomId) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/removeRoom', { roomId }, { headers })
      .map(res => res.json());
  };

  getTopRooms() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/getTopRooms', { headers })
      .map(res => res.json());
  };


  addLike(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/like',
      { userId: this.authenticationService.getCurrentUser(), postId: id },
      { headers })
      .map(res => {
        res.json();
      });
  };

  reportPost(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/report/', { id: id, user: this.authenticationService.getCurrentUser() }, { headers })
      .map(res => res.json());
  };

  updateProfile(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.post('http://localhost:3000/users/updateProfile', { data }, { headers })
      .map(res => res.json());
  };

  getUserPosts(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authenticationService.loadToken());

    return this.http.get('http://localhost:3000/users/getPosts/' + id, { headers })
      .map(res => res.json());
  };
}
