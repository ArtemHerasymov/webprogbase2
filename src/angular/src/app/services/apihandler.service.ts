import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
@Injectable()
export class ApiHandlerService {

  results: string[];


  constructor(private http: Http) { }

  private url = 'https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=8f6f1a58d16f440d93133de79a7245ad';


  // Returns 10 articles at a time
  makeHttpRequest() {
    return this.http.get('https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=8f6f1a58d16f440d93133de79a7245ad')
      .map((res: Response) => {

        let articleArray = {
          articles: {
            description: String,
            url: String,
            urlToImage: String
          }
        };
        articleArray = JSON.parse(JSON.stringify(res.json()));

        return {
          article: articleArray.articles
        }

      });
  }

  getArticlesByCategory(category) {
    console.log(`https://newsapi.org/v2/top-headlines?category=${category}&apiKey=8f6f1a58d16f440d93133de79a7245ad`);
    return this.http.get(`https://newsapi.org/v2/top-headlines?category=${category}&apiKey=8f6f1a58d16f440d93133de79a7245ad`)
      .map((res: Response) => {
        let articleArray = {
          articles: {
            description: String,
            url: String,
            urlToImage: String
          }
        };
        articleArray = JSON.parse(JSON.stringify(res.json()));
        return {
          article: (articleArray.articles)
        }
      });
  };

  getArticlesBySubstring(query) {
    return this.http.get(`https://newsapi.org/v2/everything?q=${query}&apiKey=8f6f1a58d16f440d93133de79a7245ad`)
      .map((res: Response) => {
        let articleArray = {
          articles: {
            description: String,
            url: String,
            urlToImage: String
          }
        };
        articleArray = JSON.parse(JSON.stringify(res.json()));
        return {
          article: (articleArray.articles)
        }
      })
  }

}

