import { TestBed, inject } from '@angular/core/testing';

import { ApiHandlerService } from './apihandler.service';

describe('ApihandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiHandlerService]
    });
  });

  it('should be created', inject([ApiHandlerService], (service: ApiHandlerService) => {
    expect(service).toBeTruthy();
  }));
});
