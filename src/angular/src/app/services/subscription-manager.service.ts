import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class SubscriptionManagerService {

  authenticationToken;

  constructor(private http: Http, private authService: AuthenticationService) { }



  createSubscription(subscriber, subscription) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.loadToken());
    return this.http.post('http://localhost:3000/users/subscribe',
      { subscriber: subscriber, subscription: subscription },
      { headers: headers })
      .map(res => res.json());
  };

  removeSubscription(subscriber, subscription) {
    console.log('subscriber : ' + subscriber + " subscription : " + subscription);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.loadToken());

    return this.http.post('http://localhost:3000/users/unsubscribe',
      { subscriber, subscription },
      { headers: headers }, )
      .map(res => res.json());
  };

}
