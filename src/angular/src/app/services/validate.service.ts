import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegister(user) {
    if (user.name == undefined || user.email == undefined || user.username == undefined || user.password == undefined) {
      return false;
    }
    else {
      return true;
    }
  }


  validatePassword(pass) {
    if (pass.length < 8) return false;
    return true;
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateHttp(room) {

    var tarea_regex = /^(http|https)/;

    if (tarea_regex.test(String(room.url).toLowerCase()) == true) {
      return true;
    }
    return false;
  };

  validateRoom(room) {
    if (room.title == undefined ||
      room.body == undefined ||
      room.author == undefined ||
      room.url == undefined) {
      return false;
    } else {
      return true;

    }

  }

}
