import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { ApiHandlerService } from './apihandler.service';
@Injectable()
export class RecommendationService {

  coefficient;
  recommendatedPosts = [];
  authenticationToken;

  constructor(
    private http: Http,
    private authService: AuthenticationService,
    private apiHandlerService: ApiHandlerService
  ) {
  }


  getFootprintCoeficcient() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.authService.loadToken();
    headers.append('Authorization', this.authenticationToken);
    return this.http.get('http://localhost:3000/users/getCoefficient/' + this.authService.getCurrentUser(), { headers })
      .map(res => {
        let response = res.json();
        this.coefficient = response.coefficient;
        return this.coefficient;
      })
  }

  getRecommendatedArticles() {
    let headers = new Headers();
    headers.append('Authorization', this.authService.loadToken());
    
    headers.append('Content-Type', 'application/json');
    let recs = [];
    return this.http.get('http://localhost:3000/users/getSortedSentiment/' + this.authService.getCurrentUser(), { headers })
      .map(res => res.json());
  }

  getRecommendedUsers() {
    let headers = new Headers();
    headers.append('Authorization', this.authService.loadToken());
    
    headers.append('Content-Type' , 'application/json');
    return this.http.get('http://localhost:3000/users/getRecommendedUsers/' + this.authService.getCurrentUser(), {headers})
    .map(res => {
    
      console.log(res);
      res.json();
    });
  };
}
