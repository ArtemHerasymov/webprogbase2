import { TestBed, async, inject } from '@angular/core/testing';

import { ModerationGuard } from './moderation.guard';

describe('ModerationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModerationGuard]
    });
  });

  it('should ...', inject([ModerationGuard], (guard: ModerationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
