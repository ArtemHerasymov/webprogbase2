import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
     private router: Router) {
  }

  canActivate() {

    if(this.authenticationService.isAdmin()) {
        return true;
    } else {
        this.router.navigate(['dashboard']);
        return false;
    }
}
}
